

function vbbApiKey() {
    var vbbApiKey = 'VBB_API_KEY';
    var existingApiKey = "<?php echo getConfigValue('vbb_apiKey'); ?>";
    if(!existingApiKey || vbbApiKey !== existingApiKey) {
        existingApiKey = vbbApiKey;
        $.post('setConfigValueAjax.php', {'key': 'vbb_apiKey', 'value': existingApiKey});
    }

    return existingApiKey;
}

$( function() {

  var cache = {};

  $( "#vbb_start" ).autocomplete({
    minLength: 2,
    source: function( request, response ) {
      var term = request.term;
      if ( term in cache ) {
        response( cache[ term ] );
        return;
      }

      $.getJSON('http://fahrinfo.vbb.de/restproxy/location.name?accessId=' + vbbApiKey() + '&type=S&lang=de&format=json&jsonpCallback=?&input='+term, request, function(data, status, xhr ) {
        var list = {};
        for (var i = 0; i < data.StopLocation.length; i++) {
          var stop = data.StopLocation[i];
          if($.isNumeric( stop.extId) === true) {
            var newStop = new Object();
            newStop.id = stop.extId;
            newStop.label = stop.name;
            newStop.value = stop.name;
            list[i] = newStop;
          }
        }
        cache[ term ] = list;
        response( list );
      });
    }
    });
} );

$('#vbb__edit').click(function() {
  var vbb_start = $("#vbb_start").val();
  var vbb_departuresInXMinutes = $("#vbb_departuresInXMinutes").val();
  var vbb_bus = $("#vbb_bus").is(':checked');
  var vbb_tram = $("#vbb_tram").is(':checked');
  var vbb_ubahn = $("#vbb_ubahn").is(':checked');
  var vbb_sbahn = $("#vbb_sbahn").is(':checked');
  var vbb_rb = $("#vbb_rb").is(':checked');
  var vbb_ic = $("#vbb_ic").is(':checked');
  var vbb_timeAppearanceInMinutes = $("#vbb_inMinutesAppearance").is(':checked');

  $.ajax({
  url: 'http://fahrinfo.vbb.de/restproxy/location.name?accessId=' + vbbApiKey() + '&type=S&lang=de&format=json&jsonpCallback=?&input='+vbb_start,
  jsonp: "callback",
  dataType: 'jsonp',
  success: function(data) {
      var found = false;
      for (var i = 0; i < data.StopLocation.length; i++) {
        var stop = data.StopLocation[i];
        if($.isNumeric( stop.extId ) === true
            && stop.name === vbb_start
            && vbb_departuresInXMinutes >= 2){

          $.post('setConfigValueAjax.php', {'key': 'vbb_startId', 'value': stop.id});
          $.post('setConfigValueAjax.php', {'key': 'vbb_startName', 'value': stop.name});
          $.post('setConfigValueAjax.php', {'key': 'vbb_departuresInXMinutes', 'value': Math.round(vbb_departuresInXMinutes)});
          $.post('setConfigValueAjax.php', {'key': 'vbb_bus', 'value': vbb_bus });
          $.post('setConfigValueAjax.php', {'key': 'vbb_tram', 'value': vbb_tram });
          $.post('setConfigValueAjax.php', {'key': 'vbb_ubahn', 'value': vbb_ubahn});
          $.post('setConfigValueAjax.php', {'key': 'vbb_sbahn', 'value': vbb_sbahn});
          $.post('setConfigValueAjax.php', {'key': 'vbb_rb', 'value': vbb_rb});
          $.post('setConfigValueAjax.php', {'key': 'vbb_ic', 'value': vbb_ic});
          $.post('setConfigValueAjax.php', {'key': 'vbb_timeAppearanceInMinutes', 'value': vbb_timeAppearanceInMinutes});


          $("#vbb_ok").text("Daten erfolgreich gespeichert");
          $("#vbb_error").text("");

          $('#ok').show(30, function() {
            $(this).hide('slow');
          });
          found = true;
        }
      }

      if(found === false) {
        $("#vbb_ok").text("");
        $("#vbb_error").text("Fehler, Haltestelle nicht gefunden oder Abfahrtszeit kleiner 2");
        $('#error').show(30, function() {
          $(this).hide('slow');
        });
      }
    }
  });
});
