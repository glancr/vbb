<?php

$vbb_startId = getConfigValue('vbb_startId');
$vbb_startName = getConfigValue('vbb_startName');
$vbb_departuresInXMinutes = getConfigValue('vbb_departuresInXMinutes');
$vbb_bus = getConfigValue('vbb_bus');
$vbb_tram = getConfigValue('vbb_tram');
$vbb_ubahn = getConfigValue('vbb_ubahn');
$vbb_sbahn = getConfigValue('vbb_sbahn');
$vbb_rb = getConfigValue('vbb_rb');
$vbb_ic = getConfigValue('vbb_ic');
$vbb_timeAppearanceInMinutes = getConfigValue('vbb_timeAppearanceInMinutes');

if(!$vbb_departuresInXMinutes){
    $vbb_departuresInXMinutes = "2";
}

if(!$vbb_timeAppearanceInMinutes){
    $vbb_timeAppearanceInMinutes = "true";
}


?>
<div class="ui-widget">
    <div>
        <h6><?php echo _('vbb_start'); ?></h6>
        <input type="text" id="vbb_start" value="<?php echo $vbb_startName; ?>"/>
    </div>
    <div>
        <h6><?php echo _('vbb_departures_in'); ?></h6>
        <input type="number" min="2" id="vbb_departuresInXMinutes" value="<?php echo $vbb_departuresInXMinutes; ?>"/>
    </div>
    <div>
        <h6><?php echo _('vbb_time_appearance'); ?></h6>
        <input type="radio" name="vbb_timeAppearance" id="vbb_inMinutesAppearance" <?php if ($vbb_timeAppearanceInMinutes == "true") { echo "checked";}  ?>>
        <label for="vbb_inMinutesAppearance"><?php echo _('vbb_show_in_minutes');?></label>
        <input type="radio" name="vbb_timeAppearance" id="vbb_inTimeAppearance" <?php if ($vbb_timeAppearanceInMinutes == "false") { echo "checked";}  ?>>
        <label for="vbb_inTimeAppearance"><?php echo _('vbb_show_in_time');?></label>
    </div>
    <br>
    <div>
        <input type="checkbox" id="vbb_bus" <?php if ($vbb_bus == "true") { echo "checked";}  ?> />
        <label for="vbb_bus"><?php echo _('vbb_bus'); ?></label>
    </div>
    <div>
        <input type="checkbox" id="vbb_tram" <?php if ($vbb_tram == "true") { echo "checked";}  ?> />
        <label for="vbb_tram"><?php echo _('vbb_tram'); ?></label>
    </div>
    <div>
        <input type="checkbox" id="vbb_ubahn" <?php if ($vbb_ubahn == "true") { echo "checked"; } ?> />
        <label for="vbb_ubahn"><?php echo _('vbb_ubahn'); ?></label>
    </div>
    <div>
        <input type="checkbox" id="vbb_sbahn" <?php if ($vbb_sbahn == "true") { echo "checked"; } ?> />
        <label for="vbb_sbahn"><?php echo _('vbb_sbahn'); ?></label>
    </div>
    <div>
        <input type="checkbox" id="vbb_rb" <?php if ($vbb_rb == "true") { echo "checked"; } ?> />
        <label for="vbb_rb"><?php echo _('vbb_rb'); ?></label>
    </div>
    <div>
        <input type="checkbox" id="vbb_ic" <?php if ($vbb_ic == "true") { echo "checked"; } ?> />
        <label for="vbb_ic"><?php echo _('vbb_ic'); ?></label>
    </div>
    <div>

</div>

<div id="vbb_error" style="color:red"></div>
<div id="vbb_ok" style="color:green"></div><br />

<div class="block__add" id="vbb__edit">
	<button class="vbb__edit--button" href="#">
		<span><?php echo _('save'); ?></span>
	</button>
</div>

<h6 class="vbb_terms_of_use"><?php echo _('vbb_terms_of_use'); ?></h6>
