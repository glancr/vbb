$(document).ready(function () {
    reloadDepartureBoard();
});
function productsFilter() {
    var vbb_bus = "<?php echo getConfigValue('vbb_bus'); ?>";
    var vbb_tram = "<?php echo getConfigValue('vbb_tram'); ?>";
    var vbb_ubahn = "<?php echo getConfigValue('vbb_ubahn'); ?>";
    var vbb_sbahn = "<?php echo getConfigValue('vbb_sbahn'); ?>";
    var vbb_rb = "<?php echo getConfigValue('vbb_rb'); ?>";
    var vbb_ic = "<?php echo getConfigValue('vbb_ic'); ?>";

	var resultingBitMask = 0;

    if(vbb_sbahn == 'true'){
        resultingBitMask += 1;
    }

    if(vbb_ubahn == 'true'){
        resultingBitMask += 2;
    }

    if(vbb_tram == 'true'){
        resultingBitMask += 4;
    }

    if(vbb_bus == 'true'){
        resultingBitMask += 8;
    }

    if(vbb_ic == 'true'){
        resultingBitMask += 32;
    }

    if(vbb_rb == 'true'){
        resultingBitMask += 64;
    }


    return resultingBitMask;
}

function reloadDepartureBoard() {

	var stationName = "<?php echo getConfigValue('vbb_startName'); ?>";
	var vbbStationId = "<?php echo getConfigValue('vbb_startId'); ?>";
	var vbbApiKey = "<?php echo getConfigValue('vbb_apiKey'); ?>";
	var url = "http://fahrinfo.vbb.de/restproxy/departureBoard?accessId=" + vbbApiKey + "&id=" + vbbStationId + "&format=json&jsonpCallback=?&maxJourneys=150&products=" + productsFilter();

	$("#vbb_table").empty();
	$("#vbb_subtitle").text("<?php echo _('vbb_subtitle'); ?>" + stationName + " | " + "<?php echo _('vbb_terms_of_use'); ?>");

	$.ajax({
		url: url,
        jsonp: "callback",
        dataType: 'jsonp',
		success: function (data) {
			vbbCallbackHandler(data);
		},
		error: function (error) {
			$("#vbb_error").append("error!");
		}
	});

	var everyMinute = 1000 * 60 * 1;
	window.setTimeout(function() {
        reloadDepartureBoard();
	}, everyMinute);
}

function vbbCallbackHandler(data) {
    var vbbDeparturesIn = new Number("<?php echo getConfigValue('vbb_departuresInXMinutes'); ?>");
	var departures = data.Departure;
	if(!departures.length) {
		return;
	}

	var k= 8;
	for (var i = 0; i < departures.length && k > 0; i++) {

		var departure = departures[i];
		var inThreeMinutes = new Date();
        inThreeMinutes.setMinutes(inThreeMinutes.getMinutes() + vbbDeparturesIn);

        var departureTime = new Date(departure.date);
        departureTime.setMinutes(departure.time.slice(3,5));
        departureTime.setHours(departure.time.slice(0,2));
        departureTime.setSeconds(0);

		if(inThreeMinutes < departureTime) {
			$("#vbb_table").append("<tr></tr>");
			$("#vbb_table tr:last").append("<td>" + departure.name + "</td>");
            $("#vbb_table tr:last").append("<td>" + departure.direction + "</td>");

            if("<?php echo getConfigValue('vbb_timeAppearanceInMinutes'); ?>" == 'true') {
                $("#vbb_table tr:last").append("<td>" + calculateTimeToDeparture(departureTime) + "</td>");
            } else {
                $("#vbb_table tr:last").append("<td>" + departure.time.slice(0, 5) + "</td>");
            }


			if(departure.track) {
				$("#vbb_table tr:last").append("<td>" + "<?php echo _('vbb_track'); ?>" + departure.track + "</td>");
			}
			--k;
		}
	}
}

function calculateTimeToDeparture(departureTime) {
    var departureInXMin = (departureTime.getTime() - (new Date()).getTime()) / 1000 / 60;
    return "in " + Math.round(departureInXMin) + "<?php echo _('vbb_minutes'); ?>";
}